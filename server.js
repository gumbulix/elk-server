require('dotenv').config()
const express = require('express')
const app = express()
const port = process.env.PORT

app.use(express.json());

app.get('/', (req, res) => {
    res.send('Server works!')
})

app.post('/data', (req, res) => {
    console.log(req.body)
    res.send()
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})